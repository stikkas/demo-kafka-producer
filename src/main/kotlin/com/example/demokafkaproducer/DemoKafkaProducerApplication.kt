package com.example.demokafkaproducer

import java.time.LocalTime
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.kafka.core.KafkaTemplate
import ru.tinkoff.bpm.pledge.submission.registration.process.AdditionalFields
import ru.tinkoff.bpm.pledge.submission.registration.process.StartEvent
import ru.tinkoff.bpm.pledge.submission.registration.process.UpdateSubmissionEvent

@SpringBootApplication
class DemoKafkaProducerApplication {
    //    @KafkaListener(topics = ["hello.topic"], id = "hfajoafjoajoajaoj")
    fun listen(event: String) {
        println("Hello")
    }
}

fun testRunDifferentMessage(app: ConfigurableApplicationContext) {
    val template = app.getBean(KafkaTemplate::class.java) as KafkaTemplate<String?, StartEvent?>
    val updateTemplate = app.getBean(KafkaTemplate::class.java) as KafkaTemplate<String?, UpdateSubmissionEvent?>
    val now = LocalTime.now().minute
    for (i in 1..5) {
        val id = i + now
        template.send(
            "pledge.submission-registration.process",
            StartEvent(i.toString(), "I don't know $i", id.toString(), AdditionalFields())
        )
        updateTemplate.send(
            "pledge.submission-registration.process",
            UpdateSubmissionEvent(id.toString())
        )
    }
}

fun testRunStartEvent(template: KafkaTemplate<String?, StartEvent>) {
    template.send(
        "pledge.submission-registration.process",
        StartEvent(
            "AUTO202099q9010z11VSQ2130Ns",
            "SUBMISSION",
            "20210611120656792PLES5c9cb9685",
            AdditionalFields("3202219-456564", 1623409245, 1623409245, "4545445")
        )
    )

}

fun testPmpStartEvent(template: KafkaTemplate<String?, ru.tinkoff.bpm.pledge.mortgage.StartEvent>) {
    template.send(
        "bpm.pledge-mortgage.create",
        ru.tinkoff.bpm.pledge.mortgage.StartEvent(
            "AUTO20210903171112VSQ294401",
            "20220713171544871PLESa1cff6766",
            "AUTO20210903171112VSQ294401_61",
            154580
        )
    )
}

fun main(args: Array<String>) {
    val app = runApplication<DemoKafkaProducerApplication>(*args)
//    app.getBean(ConsumerFactory::class.java).let {
//        Logger.getLogger("DemoKafkaProducerApplication").info(it.configurationProperties.toString())
//    }
//    testRunStartEvent(app.getBean(KafkaTemplate::class.java) as KafkaTemplate<String?, StartEvent>)
    testPmpStartEvent(app.getBean(KafkaTemplate::class.java) as KafkaTemplate<String?, ru.tinkoff.bpm.pledge.mortgage.StartEvent>)
}
